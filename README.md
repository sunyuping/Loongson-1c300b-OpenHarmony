# OpenHarmony龙芯1C300B

#### 介绍
OpenHarmony是由开放原子开源基金会（OpenAtom Foundation）孵化及运营的开源项目，目标是面向全场景、全连接、全智能时代，搭建一个智能终端设备操作系统的框架和平台，促进万物互联产业的繁荣发展。

OpenHarmony通过组件化和组件弹性化等设计方法，做到硬件资源的可大可小，在多种终端设备间，按需弹性部署。目前OpenHarmony最新的3.0代码已支持ARM、RISC-V、x86等各种CPU。但是目前OpenHarmony轻量系统还不支持MIPS架构的芯片，更不支持龙芯。

慧睿思通经过公司的努力，成功地将OpenHarmny移植到龙芯1c300处理器上，实现了国内第一家成功移植OpenHarmny轻量系统到龙芯、MIPS架构芯片的公司。

该代码开源，使用过程中有问题请留言或者联系我司：13512779286 饶经理 或者 13510979604 连志安

#### 龙芯1C300芯片介绍
1C300是龙芯基于 GS232 处理器核的高性价比单芯片系统，内置浮点处理单元，支持多种类型内存，支持大容量 MLC 闪存芯片，可应用于工控、物联网等领域。

#### OpenHarmony版本说明
本仓库是基于OpenHarmony 3.0版本移植，通过裁剪，去掉不必要的组件，精简版本。
1c300裸机库基于勤为本的OpenLoongsonLib1c，感谢！

#### 安装教程

1.  下载交叉编译工具链
龙芯1C300编译器用来编译鸿蒙系统 龙芯1C库可以在Linux下与Windows下使用 MIPS.com 提供的 CodeScape MTI Bare Metal Toolchain 进行交叉编译 注意，请选择“MTI Bare Metal Toolchain”下的

[MIPS官网的交叉编译工具链下载地址](http://codescape.mips.com/components/toolchain/2017.10-07/downloads.html)

下载后把交叉编译添加到自己的环境变量中去

2.  OpenHarmony开发环境
按照OpenHarmony官方提供的环境搭建，配置号gn、hb等环境，可以参考：
[获取源码及Ubuntu编译环境准备](https://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/quick-start/quickstart-lite-env-setup-linux.md)

3.  开始编译
hb set
选择ls1c300b_hrst
执行 hb build -f
看到如下提示说明编译通过：
[OHOS INFO] ls1c300b_hrst build success
生成的烧录固件位于文件夹：out\ls1c300b_hrst\ls1c300b_hrst
固件：ls1c300b_hrst_ninjia.elf 

4.  烧录
配置好tftp，进入PMON，执行 load tftp://192.168.1.100/ls1c300b_hrst_ninjia.elf
下载完固件后，输入
g
开始执行。

5.  查看现象
查看串口打印，如下：

```
entering kernel init...
Lian Zhian 13510979604
Open Harmony 3.0 start ...

Entering scheduler
                  ___>>>> start task TaskSampleEntry1
___>>>>>> TaskSampleEntry1 ../../../device/loongson/ls1c300b_hrst/test/my_test.c 29

___>>>> start task gpio_task
___>>>> start task key_irq_task
___>>>> start task TaskSampleEntry2
___>>>>>> TaskSampleEntry2 ../../../device/loongson/ls1c300b_hrst/test/my_test.c 19

___>>>>>> TaskSampleEntry1 ../../../device/loongson/ls1c300b_hrst/test/my_test.c 29

```
查看LED灯，可以看到1秒闪1次。


#### 使用说明

目前已经完成了轻量内核移植，多任务、中断、systick都没问题。
还有一些驱动未适配、以及网卡、FPU功能未实现，也欢迎感兴趣的朋友一起加入到慧睿思通开源社区，共同维护、更新该开源仓库。可以添加13510979604（连）参与共建。

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
