#include "los_config.h"
#include "los_debug.h"
#include "los_interrupt.h"
#include "los_task.h"
#include "los_tick.h"

#include "ls1c_public.h"
#include "ls1c_gpio.h"
#include "ls1c_delay.h"
#include "ls1c_irq.h"



VOID key_task(VOID)
{
    unsigned int key_gpio = 85;     
    unsigned int led_gpio = 32;     

    printf("___>>>> start task %s\r\n", __FUNCTION__);
    
    gpio_init(key_gpio, gpio_mode_input);

    while (1) {

        LOS_TaskDelay(10);
        if (gpio_level_low != gpio_get(key_gpio))
            continue;

        
        LOS_TaskDelay(10);
        if (gpio_level_low != gpio_get(key_gpio))
            continue;       

        
        printf("key_task val is %d\r\n", gpio_get(key_gpio));

        
        while (gpio_level_high != gpio_get(key_gpio));
        
        LOS_TaskDelay(10);

        printf("key_task val is %d\r\n", gpio_get(key_gpio));
    }
}


VOID gpio_task(VOID)
{
    unsigned int led_gpio = 32;

    printf("___>>>> start task %s\r\n", __FUNCTION__);
    
    led_init(led_gpio);

    while (1)
    {
        led_on(led_gpio);
        LOS_TaskDelay(1 * 1000);

        led_off(led_gpio);
        LOS_TaskDelay(1 * 1000);
    }
}


void create_gpio_task(void)
{
    UINT32 uwRet;
    UINT32 taskID1;
    TSK_INIT_PARAM_S stTask = {0};

    stTask.pfnTaskEntry = (TSK_ENTRY_FUNC)gpio_task;
    stTask.uwStackSize = 0x1000;
    stTask.pcName = "gpio_task";
    stTask.usTaskPrio = 6; /* Os task priority is 6 */
    uwRet = LOS_TaskCreate(&taskID1, &stTask);
    if (uwRet != LOS_OK) {
        printf("gpio_task create failed\r\n");
    }
}

void create_key_task(void)
{
    UINT32 uwRet;
    UINT32 taskID1;
    TSK_INIT_PARAM_S stTask = {0};

    stTask.pfnTaskEntry = (TSK_ENTRY_FUNC)key_task;
    stTask.uwStackSize = 0x1000;
    stTask.pcName = "gpio_task";
    stTask.usTaskPrio = 6; /* Os task priority is 6 */
    uwRet = LOS_TaskCreate(&taskID1, &stTask);
    if (uwRet != LOS_OK) {
        printf("key_task create failed\r\n");
    }
}