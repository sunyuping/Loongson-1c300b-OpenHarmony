
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "../lib/ls1c_public.h"
#include "../lib/ls1c_irq.h"
#include "../lib/ls1c_gpio.h"
#include "../lib/ls1c_delay.h"
#include "../lib/ls1c_mipsregs.h"
#include "../lib/ls1c_uart.h"
#include "../lib/ls1c_sys_tick.h"
#include "../lib/ls1c_clock.h"
#include "../example/test_gpio.h"
#include "../example/test_pwm.h"
#include "../example/test_delay.h"
#include "../example/test_simulate_i2c.h"
#include "../example/test_timer.h"
#include "../example/test_fpu.h"
#include "../example/test_i2c.h"
#include "../example/test_uart.h"
#include "../example/test_sys_tick.h"
#include "../example/test_spi.h"



#define ENABLE_SAMPLE_TASK              1
#define ENABLE_GPIO_TASK                    1
#define ENABLE_KEY_TASK                     0
#define ENABLE_KEY_IRQ_TASK             1

VOID run_los(void);

// ç¡¬æµ®ç‚¹åˆå§‹åŒ–
void fpu_init(void)
{
    unsigned int c0_status = 0;
    unsigned int c1_status = 0;

    // 使能协处理器1--FPU
    c0_status = read_c0_status();
    c0_status |= (ST0_CU1 | ST0_FR);
    write_c0_status(c0_status);
    // é…ç½®FPU
    c1_status = read_c1_status();
    c1_status |= (FPU_CSR_FS | FPU_CSR_FO | FPU_CSR_FN);    // set FS, FO, FN
    c1_status &= ~(FPU_CSR_ALL_E);                          // disable exception
    c1_status = (c1_status & (~FPU_CSR_RM)) | FPU_CSR_RN;   // set RN
    write_c1_status(c1_status);

    return ;
}


void bsp_init(void)
{
#if 1
    // 初始化调试串口
    uart2_init();
    printf("_____>>>>> %s %d\r\n", __FILE__, __LINE__);
    // 硬浮点初始化
    fpu_init();
    printf("_____>>>>> %s %d\r\n", __FILE__, __LINE__);
    // 初始化异常
    exception_init();
    printf("_____>>>>> %s %d\r\n", __FILE__, __LINE__);
    // 显示时钟信息
    clk_print_all();
    printf("_____>>>>> %s %d\r\n", __FILE__, __LINE__);
#endif
    return ;
}


int main(void)
{
    bsp_init();
    printf("_____>>>>> %s %d\r\n", __FILE__, __LINE__);

    run_los();

    while (1)
        ;

	return(0);
}

void create_test_task(void)
{
    #if (ENABLE_SAMPLE_TASK == 1)
    TaskSample();
    #endif

    #if (ENABLE_GPIO_TASK == 1)
    create_gpio_task();
    #endif

    #if (ENABLE_KEY_TASK == 1)
    create_key_task();
    #endif

    #if (ENABLE_KEY_IRQ_TASK == 1)
    create_key_irq_task();
    #endif
}

VOID run_los(void)
{
    UINT32 ret;
    ret = LOS_KernelInit();

    printf("Lian Zhian 13510979604\r\n");
    printf("Open Harmony 3.0 start ...\r\n\r\n");

    if (ret == LOS_OK) 
    {
        create_test_task();
        
        //Æô¶¯OpenHarmony OS ÄÚºË
        LOS_Start();
    }
}


